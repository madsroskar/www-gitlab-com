---
layout: markdown_page
title: "Handbook Separation"
description: "Separation of the handbook from the www-gitlab-com repository"
canonical_path: "/company/team/structure/working-groups/handbook-separation/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value             |
|-----------------|-------------------|
| Date Created    |  |
| Target End Date |  |
| Slack           | [#wg_handbook_separation](https://app.slack.com/client/T02592416/C01V31T18E9) |
| Google Doc      | [Working Group Agenda](https://docs.google.com/document/d/1615ZnElQh3qy96dbRegn-vuU2VBLCxh9pbEf88iL3TE/edit?usp=sharing)|
| Issue Board | [Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/2660335) |
| Label           | `~wg-handbook-separation` |

## Problems To Solve

Pulling the handbook into a separate repository and removing it from the `www-gitlab-com` repository. 

### Context
For more context on please reference the following epics created prior to creation of this working group:

1. [www-gitlab-com improvements & support](https://gitlab.com/groups/gitlab-com/-/epics/423)
1. [Monorepo refactor :: Phase 1.5 - Handbook Isolation epic](https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/316)
1. [Define DRI for `www-gitlab-com` repo and project](https://gitlab.com/gitlab-com/Product/-/issues/1869) & [supporting google doc](https://docs.google.com/document/d/1h6m4FQMRcqbSQ6TiRAFFVV52rlLBIKspkSgJ6UD_Ico/edit#)

## Business Goals

- Enable GitLab to sell handbook as a feature  
- Provide a clear path to making updates to the handbook using GitLab editor tools
- Enable the tooling of each repository to diverge specific to their needs.
- Faster pipelines for each project
- Efficient builds and reduction in CI minutes required for each project
- Boost GitLab team productivity 
- Provide a clear separation of concerns for the Digital Experience team
- More TBD

## Protocols and Processes

**Creating Epics, Issues and MRs for the working group board**

- All epics should have labels: `mktg-website` `wg-handbook-separation`
- All issues should be linked to a WG epic and have labels: `mktg-website` `wg-handbook-separation`, the issues will be viewable as part of the [Handbook Separation Working Group issue board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/2660335)
- All MRs should be linked to a WG issue and have the labels: `mktg-website` `wg-handbook-separation`

Please note the above actions are necessary for succesful async collaboration. 

**How we'll review and share the work**

- Please  provide a standup style update of your work in progress at the biweekly sync by adding it to the [agenda](https://docs.google.com/document/d/1615ZnElQh3qy96dbRegn-vuU2VBLCxh9pbEf88iL3TE/edit?usp=sharing) and then voicing it over. If you cannot attend the biweekly sync, please still add the written update and the WG facilitator will voice over. 
- Please explicitly communicate any reviews, feedback or contributions you need from the bigger WG team by adding an agenda item to weekly sync meeting or by posting in the WG Slack channel, in addition to pinging directly as appropriate in the epic, issue or MR. Highlighting in weekly sync and Slack are critical  as we all have many pings and ToDos in GitLab daily. Please make any due dates for feedback clear as will help the WG prioritize when you need feedback. 
- Please add  the WG facilitator `TBD` as a reviewer on all MRs as DRI and to also allow additonal labeling.
- Please add required reviewers of your MR by assigning them as  `reviewer`
- Please unassign yourself as `reviewer` when you've completed feedback 

**NOTE TO ALL: Please pay attention to your GitLab To-Do list! We'll not hold the train on non-controversial MRs for more than 24 hours.**

## Meetings

Meetings are recorded and available on GitLab unfiltered and a handbook separation working group playlist. The faciliator will post a link to the video in a Slack as part of a  meeting summary after every weekly sync.

## Starting Criteria
1. Designate DRI for the handbook
1. TBD

## Exit Criteria

1. Separate repository for the handbook.
1. TBD

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     |  | |
| Facilitator           |  | |
| Functional Lead       |  | |
| Member       | Lauren Barker | Senior Fullstack Engineer, Digital Experience |
| Member       | Chad Wooley | Senior Engineer, Product |
| Member       | Tyler Williams | Fullstack Engineer, Digital Experience|
| Member | Lien Van Den Steen | Senior Fullstack Engineer, People Group |
