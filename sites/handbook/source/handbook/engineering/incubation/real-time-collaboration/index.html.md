---
layout: handbook-page-toc
title: Real Time Collaboration Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Real Time Collaboration Single-Engineer Group

The Real Time Collaboration SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

Real Time Collaboration for GitLab is the concept of seeing people interacting with a merge request in real time.  This effort will build upon the recent work to [view real-time updates of assignee in issue sidebar](https://gitlab.com/gitlab-org/gitlab/-/issues/17589) to see if there is customer value in providing the following functionality:

* allow people with developer permissions and above to see if someone else with developer permissions or above is looking at the same merge request/issue/etc
* start a collaboration session which allows Google Docs style typing in text fields (comments, issue descriptions etc)
* allow people collaborating in the same session to easily follow each other to other issues and merge requests etc

More detail is available in the [Real-time collaboration](https://gitlab.com/groups/gitlab-org/-/epics/2345) Epic.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/21473](https://gitlab.com/gitlab-org/gitlab/-/issues/21473)

### Vacancy

We’re currently hiring for this role and looking for someone that understands the underlying technologies used in real-time collaboration solutions in order to help design and develop this feature.  

You’ll need experience in bringing products to markets, experience with similar use cases, and experience with developing large scale services.  Ideally you've worked with WebSockets on Rails, and have experience (or at least knowledge and a keen interest) in [CFRDTs]
(https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type)

You should know the different technological approaches and open source solutions in this space, and be able to architect a scalable approach that is integrated into GitLab.  Our tech stack is Ruby, Go and Vue.js, and you’ll need to work across backend, frontend database and infrastructure to bring this opportunity to market.  

You can apply on our [careers page](https://about.gitlab.com/jobs/careers/).

### Reading List

* [Hybrid Anxiety and Hybrid Optimism: The Near Future of Work](https://future.a16z.com/hybrid-anxiety-optimism-future-of-work/)
